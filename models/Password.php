<?php

namespace app\models;

use app\components\BaseActiveRecord;
use Yii;

/**
 * This is the model class for table "password".
 *
 * @property integer $id
 * @property string $password
 * @property string $description
 */
class Password extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password';
    }

    public function scenarios() {
        return array_merge(
            parent::scenarios(), [
                self::SCENARIO_CREATE =>['password','description','project_id'],
                self::SCENARIO_UPDATE =>['password','description'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['project_id'], 'integer'],
            [['password'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 64]
        ];
    }

    public function getProject() {
        return $this->hasOne(Project::className(),['project_id'=>'id']);
    }

    public function getUserProject() {
        return $this->hasMany(UserProject::className(),['project_id'=>'project_id']);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Password',
            'description' => 'Description',
            'project_id'=> 'Project ID'
        ];
    }

    public function fields() {
        return [
            'id',
            'description',
            'password',
            'project_id'
        ];
    }
}
