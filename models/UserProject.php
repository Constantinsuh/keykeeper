<?php

namespace app\models;

use app\components\BaseActiveRecord;
use Yii;

/**
 * This is the model class for table "user_project".
 *
 * @property integer $user_id
 * @property integer $project_id
 */
class UserProject extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_project';
    }

    public function getPasswords() {
        return $this->hasMany(Password::className(), ['project_id'=>'project_id']);
    }

    public function getProject() {
        return $this->hasOne(Project::className(), ['id'=>'project_id']);
    }

    public function scenarios() {
        return array_merge(
            parent::scenarios(), [
                self::SCENARIO_CREATE =>['user_id','project_id','can_view','can_edit'],
                self::SCENARIO_UPDATE =>['can_view', 'can_edit'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'project_id'], 'required'],
            [['user_id', 'project_id', 'can_view', 'can_edit'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'project_id' => 'Project ID',
        ];
    }
}
