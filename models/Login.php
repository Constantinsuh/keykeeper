<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/23/2016
 * Time: 7:00 PM
 */

namespace app\models;


use app\components\UserIdentity;
use yii\base\Model;

class Login  extends Model{

    public $login;
    public $password;

    private $_user = false;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || $user->password != md5($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return \Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findOne(['login'=>$this->login]);
        }
        return $this->_user;
    }
} 