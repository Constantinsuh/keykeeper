<?php

namespace app\models;

use app\components\BaseActiveRecord;
use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 */
class Project extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    public function getUserProject() {
        return $this->hasMany(UserProject::className(),['project_id'=>'id']);
    }

    public function scenarios() {
        return array_merge(
            parent::scenarios(), [
                self::SCENARIO_CREATE =>['name'],
                self::SCENARIO_UPDATE =>['name'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert === true) {
            $userProject = new UserProject();
            $userProject->setAttributes([
                'project_id' => $this->id,
                'user_id' =>  \Yii::$app->user->getId(),
                'can_view' => 1,
                'can_edit' => 1
            ]);
            $userProject->save();
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->deleteRelatedUsersProjects();
        $this->deleteRelatedPasswords();

    }

    public function deleteRelatedUsersProjects() {
        $usersProjects = UserProject::find()->where(['project_id'=>$this->id])->all();
        foreach($usersProjects as $item) {
            $item->delete();
        }
    }

    public function deleteRelatedPasswords() {
        $passwords = Password::find()->where(['project_id'=>$this->id])->all();
        foreach($passwords as $item) {
            $item->delete();
        }
    }
}
