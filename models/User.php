<?php

namespace app\models;

use app\components\BaseActiveRecord;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $access_token
 */
class User extends BaseActiveRecord
{
    public $passwordConfirm;

    public static function tableName()
    {
        return 'user';
    }

    public function scenarios() {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_CREATE =>['name','login','email','password','passwordConfirm'],
                self::SCENARIO_UPDATE =>['name','login','email','password'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'login'], 'required'],
            [['name', 'login'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 64,],
            [['password','passwordConfirm'], 'string', 'max' => 128],
            [['password','passwordConfirm'], 'required', 'on'=>self::SCENARIO_CREATE],
            [['password'], 'compare', 'compareAttribute'=>'passwordConfirm', 'on'=>self::SCENARIO_CREATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'login' => 'Login',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    public function fields() {
        return [
            'id','name','login','email'
        ];
    }

    /*
     * if $this->password === null, set to him old password;
     * if $this->password was changed, create hash from new password
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->login == 'admin') {
                Yii::$app->getResponse()->setStatusCode(400, 'Do not change admin, please :)');
                return false;
            }
            if ($this->isNewRecord) {
                $this->access_token = \Yii::$app->security->generateRandomString();
                $this->password = md5($this->password);
            } else {
                if(empty($this->password) ) {
                    $this->setAttribute('password',$this->getOldAttribute('password'));
                } else if(array_key_exists('password', $this->dirtyAttributes) ) {
                    $this->password = md5($this->password);
                }
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert) {
            $auth = Yii::$app->authManager;
            $userRole = $auth->getRole('userRole');
            $auth->assign($userRole, $this->id);
        }
    }
}
