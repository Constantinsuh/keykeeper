<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/24/2016
 * Time: 5:08 PM
 */

namespace app\commands;


use app\rbac\rules\PasswordRule;
use app\rbac\rules\ProjectRule;
use app\rbac\rules\UserProjectRule;
use app\rbac\rules\UserRule;
use yii\console\Controller;
use yii\rbac\Rule;

class AuthorRule extends Rule
{
    public function execute($u,$i,$p) {

    }
}

class RbacController extends Controller {

    public $auth;

    public function actionInit() {
        $this->auth = $auth = \Yii::$app->authManager;
        $auth->removeAllPermissions();
        $auth->removeAllRoles();
        $auth->removeAllRules();

        $user = $auth->createRole('userRole');
        $admin = $auth->createRole('admin');

        $auth->add($user);
        $auth->add($admin);
        $auth->addChild($admin, $user);

        $userPermission = $auth->createPermission('userPermission');
        $passwordPermission = $auth->createPermission('passwordPermission');
        $projectPermission = $auth->createPermission('projectPermission');
        $userProjectPermission = $auth->createPermission('userProjectPermission');

        $auth->add($userPermission);
        $auth->add($passwordPermission);
        $auth->add($projectPermission);
        $auth->add($userProjectPermission);

        $auth->addChild($user, $userPermission);
        $auth->addChild($user, $passwordPermission);
        $auth->addChild($user, $projectPermission);
        $auth->addChild($user, $userProjectPermission);

        $userRule = new UserRule();
        $passwordRule = new PasswordRule();
        $projectRule = new ProjectRule();
        $userProjectRule = new UserProjectRule();

        $userPermission->ruleName = $userRule->name;
        $passwordPermission->ruleName = $passwordRule->name;
        $projectPermission->ruleName = $projectRule->name;
        $userProjectPermission->ruleName = $userProjectRule->name;

        $auth->add($userRule);
        $auth->add($passwordRule);
        $auth->add($projectRule);
        $auth->add($userProjectRule);

        $userPermissionInnerPermission = $auth->createPermission('user');
        $passwordPermissionInnerPermission = $auth->createPermission('password');
        $projectPermissionInnerPermission = $auth->createPermission('project');
        $userProjectPermissionInnerPermission = $auth->createPermission('user-project');

        $auth->add($userPermissionInnerPermission);
        $auth->add($passwordPermissionInnerPermission);
        $auth->add($projectPermissionInnerPermission);
        $auth->add($userProjectPermissionInnerPermission);

        $auth->addChild($userPermission,$userPermissionInnerPermission);
        $auth->addChild($passwordPermission,$passwordPermissionInnerPermission);
        $auth->addChild($projectPermission,$projectPermissionInnerPermission);
        $auth->addChild($userProjectPermission,$userProjectPermissionInnerPermission);

    }

} 