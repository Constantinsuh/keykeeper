<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/24/2016
 * Time: 7:26 PM
 */

namespace app\rbac;


use app\components\BaseRule;
use yii\base\Exception;
use yii\rbac\Item;
use yii\rbac\Rule;

class UsersPasswordsRule extends BaseRule {

    public $name = 'UsersPasswordsRule';

    /*
     * Check all requested models, for user access;
     */
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if(parent::execute($user, $item, $params) === true) {
            return true;
        }

        return true;

        $model = $params['model'];
        if(($model === null)) {
            throw new Exception('$params->model is required');
        }
        if(count($model) > 1 ) {
            foreach($model as $item) {
                if($item->user_id !== $user) {
                    return false;
                }
            }
        } else {
            if($model->user_id !== $user) {
                return false;
            }
        }
        return true;

    }
}