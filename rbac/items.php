<?php
return [
    'userRole' => [
        'type' => 1,
        'children' => [
            'userPermission',
            'passwordPermission',
            'projectPermission',
            'userProjectPermission',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'userRole',
        ],
    ],
    'userPermission' => [
        'type' => 2,
        'ruleName' => 'UserRule',
        'children' => [
            'user',
        ],
    ],
    'passwordPermission' => [
        'type' => 2,
        'ruleName' => 'PasswordRule',
        'children' => [
            'password',
        ],
    ],
    'projectPermission' => [
        'type' => 2,
        'ruleName' => 'ProjectRule',
        'children' => [
            'project',
        ],
    ],
    'userProjectPermission' => [
        'type' => 2,
        'ruleName' => 'UserProjectRule',
        'children' => [
            'user-project',
        ],
    ],
    'user' => [
        'type' => 2,
    ],
    'password' => [
        'type' => 2,
    ],
    'project' => [
        'type' => 2,
    ],
    'user-project' => [
        'type' => 2,
    ],
];
