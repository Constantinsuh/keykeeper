<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/31/2016
 * Time: 4:25 PM
 */

namespace app\rbac\rules;

use app\components\BaseRule;
use app\models\UserProject;
use yii\rbac\Rule;
use yii;
use yii\base\exception;

class PasswordRule extends BaseRule {

    public $name = 'PasswordRule';

    public function execute($user, $item, $params) {
        if(parent::execute($user, $item, $params) === true) {
            return true;
        }
        $model = $params['model'];

        if(is_object($model)) {
            if ($this->checkModel($model)) {
                return true;
            }
        } else if (is_array($model)) {
            foreach($model as $mod) {
                if(!$this->checkModel($mod)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function checkModel($model) {
        $userProject = UserProject::find()
            ->where(['project_id'=>$model->project_id, 'user_id'=>Yii::$app->user->id])
            ->all();
        if(count($userProject) > 1) {
            throw new Exception('More then one row with the same user_id('.Yii::$app->user->id.') and project_id('.$model->project_id.') in user_project');
        }

        $userProject = $userProject[0];
        if(!empty($userProject)) {
            switch($this->action) {
                case 'create':
                case 'delete';
                case 'update':
                    return $userProject->can_edit == 1 ? true : false;
                case 'view':
                case 'index':
                    return $userProject->can_view == 1 ? true : false;
                default:
                    throw new Exception('Unknown method "'.$this->action.'"');
            }
        }
        return false;
    }

}