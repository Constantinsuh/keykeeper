<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/30/2016
 * Time: 1:07 PM
 */

namespace app\components;


use yii\db\ActiveRecord;

class BaseActiveRecord extends ActiveRecord {

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /*
     * Declare probables scenarios and by default set no attributes for safe assignment
     */
    public function scenarios() {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_CREATE =>[],
                self::SCENARIO_UPDATE =>[]
            ]
        );
    }

} 