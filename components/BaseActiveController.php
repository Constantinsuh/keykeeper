<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/18/2016
 * Time: 7:14 PM
 */

namespace app\components;


use app\models\Password;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ServerErrorHttpException;
use yii;
use yii\base\Exception;

class BaseActiveController extends ActiveController {

    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
            ]
            ]
        );
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['delete']);
        unset($actions['update']);
        return $actions;
    }

    public function beforeAction($action) {
        if(parent::beforeAction($action)) {
            return true;
        }
        return false;
    }

    public function actionIndex() {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()->all();

        if( Yii::$app->user->can($this->id,['model'=>$model])) {
            return $model;
        } else {
            throw new Exception('Not enough right');
        }
    }

    public function actionView($id) {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        if( Yii::$app->user->can($this->id,['model'=>$model])) {
            return $model->getAttributes();
        } else {
            throw new Exception('Not enough right');
        }
    }

    public function actionCreate() {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = new $modelClass();
        $model->setScenario($model::SCENARIO_CREATE);

        $model->setAttributes(Yii::$app->getRequest()->getBodyParams());
        if( Yii::$app->user->can($this->id,['model'=>$model])) {
            if($model->save()) {
                Yii::$app->getResponse()->setStatusCode(201);
                return $model->getAttributes($model->fields());
            } else {
                if(!$model->validate()) {
                    $errors = '';
                    foreach($model->getFirstErrors() as $error ) {
                        $errors .= $error.' ';
                    }
                    Yii::$app->getResponse()->setStatusCode(400, $errors);
                    return $model->getErrors();
                } else {
                    throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
                }
            }
        } else {
            throw new Exception('Not enough right');
        }
    }


    public function actionDelete($id) {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        if( Yii::$app->user->can($this->id,['model'=>$model])) {
            $model->delete();
            Yii::$app->getResponse()->setStatusCode(204);
            return true;
        } else {
            throw new Exception('Not enough right');
        }
    }

    public function actionUpdate($id) {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);
        if(!$model) {
            Yii::$app->response->setStatusCode(404);
            return false;
        }
        $model->setScenario($model::SCENARIO_UPDATE);

        if( Yii::$app->user->can($this->id,['model'=>$model])) {
            $model->load(Yii::$app->request->getBodyParams(), '');
            if($model->save()) {
                return $model;
            } else {
                if(!$model->validate()) {
                    $errors = '';
                    foreach($model->getFirstErrors() as $error ) {
                        $errors .= $error.' ';
                    }
                    Yii::$app->getResponse()->setStatusCode(400, $errors);
                    return $model->getErrors();
                } else {
                    throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
                }
            }
        } else {
            throw new Exception('Not enough right');
        }
    }
} 