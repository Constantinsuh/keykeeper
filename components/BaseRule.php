<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 4/1/2016
 * Time: 12:34 PM
 */

namespace app\components;


use yii\rbac\Rule;
use yii\base\exception;
use yii;

class BaseRule extends Rule {

    public $name = 'UserProjectRule';
    public $action;

    public function execute($user, $item, $params) {
        $this->action = Yii::$app->controller->action->id;
        foreach(Yii::$app->authManager->getRolesByUser($user) as $role) {
            if($role->name === 'admin') {
                return true;
            }
        }
        return false;
    }
}
