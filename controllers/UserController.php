<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/29/2016
 * Time: 6:14 PM
 */

namespace app\controllers;


use app\components\BaseActiveController;
use yii\base\Exception;

class UserController extends BaseActiveController {

    public $modelClass = 'app\models\User';

    public function actionIndex() {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()->all();
        if( \Yii::$app->user->can($this->id,['model'=>$model])) {
            return $model;
        } else {
            throw new Exception('Not enough right');
        }
    }

} 