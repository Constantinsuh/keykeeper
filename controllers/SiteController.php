<?php
//admin
//password
namespace app\controllers;

use app\models\Login;
use app\models\Project;
use app\models\UserProject;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;

class SiteController extends Controller
{
    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except'=>['login', 'index'],
            ]
            ]
        );
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionLogin() {
        $postData = Yii::$app->request->post();
        if(isset($postData['login']) && isset($postData['password'])) {
            $user = new Login();
            $user->login = $postData['login'];
            $user->password = $postData['password'];
            if($user->login()) {
                echo json_encode([
                    'result'=>'success',
                    'access_token' => Yii::$app->user->identity->getAuthKey()
                ]);
            } else {
                $errors = '';
                foreach($user->getFirstErrors() as $error ) {
                    $errors .= $error.' ';
                }
                Yii::$app->getResponse()->setStatusCode(400, $errors);
                return;
            }
        }
    }

    public function actionGetUserInfo() {
        $u = Yii::$app->user->identity;
        $userInfo= [];
        $userInfo['name'] = $u->name;
        $userInfo['id'] = $u->id;
        $roles =  Yii::$app->authManager->getRolesByUser($u->id);
        foreach ($roles as $role) {
            $userInfo['roles'][] = $role->name;
        }
        if(empty($roles)) {
            $userInfo['roles'][] = 'user';
        }
        return json_encode($userInfo);
    }

}
