<?php
/**
 * Created by PhpStorm.
 * User: Kotya
 * Date: 3/28/2016
 * Time: 3:34 PM
 */

namespace app\controllers;


use app\components\BaseActiveController;
use yii\base\Exception;
use yii\data\ActiveDataProvider;

class UserProjectController extends BaseActiveController {

    public $modelClass = 'app\models\UserProject';
    public $filterField = 'user_id';

    public function actionIndexByUser($userId = null) {
        /* @var $model \app\components\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()
            ->where([$this->filterField => $userId])
            ->all();

        if( \Yii::$app->user->can($this->id,['model'=>$model])) {
            return $model;
        } else {
            throw new Exception('Not enough right');
        }
    }


    /**
     * Method is not require user->can(), because not depends from user data (POST, GET)
     * @return ActiveDataProvider  only own rows
     */
    public function actionGetOwnProjects() {
        $relations = ['passwords','project'];

        /* @var $modelClass \app\models\UserProject */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()
            ->joinWith($relations)
            ->where([
                $modelClass::tableName().'.'.$this->filterField => \Yii::$app->user->id,
                $modelClass::tableName().'.can_view'=> 1
            ])
            ->all();
        $arr = [];

        if(!empty($relations)) {
            foreach($model as $mod) {
                $arr[] =  array_merge($mod->getRelatedRecords(), $mod->getAttributes());
            }
        } else {
            $arr = $model;
        }
        return $arr;
    }
} 