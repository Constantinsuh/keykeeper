<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
?>
<?php $this->beginPage() ?>
<?php AppAsset::register($this); ?>
<!DOCTYPE html>
<html ng-app="app" lang="<?= Yii::$app->language ?>">
<head>

    <meta name="csrf-token" content="<?=Yii::$app->request->getCsrfToken()?>">
    <title><?= Html::encode($this->title) ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>


<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
