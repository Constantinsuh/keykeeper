<?php ?>

<div class="main-wrap" ng-controller="MainController">
    <div class="">
        <nav>
            <ul class="nav nav-tabs text-center">

                <li ng-show="isLogged()"><a href="#/keys" >Keys</a></li>
                <li ng-show="isAdmin()"><a href="#/createUser" >Create user</a></li>
                <li ng-show="isAdmin()"><a href="#/editPermissions">Edit permissions</a></li>
                <li ng-show="!isLogged()" ><a href="#/login" >Login</a></li>
                <li ng-show="isLogged()"><a href="#" ng-click="onLogout()" >Log out ({{userInfo.name}})</a></li>
<!--                <li><a href="#/test">Test permissions</a></li>-->
            </ul>
        </nav>
        <div class="ver-margin-25"></div>
        <div ng-view></div>


    </div>

    <div class="bottom-alert">
        <div ng-repeat="alert in alertBottomMessages">
            <div  class="text-size-20" >
                <div ng-attr-class="{{alert.type}}">
                    {{alert.text}}
                </div>
            </div>
        </div>

    </div>

</div>
