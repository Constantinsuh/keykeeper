<?php

use yii\db\Migration;

class m160330_124218_user_project_can_edit extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_project','can_edit',$this->boolean()->defaultValue(0)->notNull());
        $this->addColumn('user_project','can_view',$this->boolean()->defaultValue(0)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('user_project','can_edit');
        $this->dropColumn('user_project','can_view');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
