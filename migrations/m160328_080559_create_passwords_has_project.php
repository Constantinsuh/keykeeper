<?php

use yii\db\Migration;

class m160328_080559_create_passwords_has_project extends Migration
{
    public function safeUp()
    {
        $this->createTable('projects_passwords', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'user_id'=>$this->integer(11),
        ]);

        $this->addColumn('users_passwords','project_id','int(11) not null');
    }

    public function safeDown()
    {
        $this->dropTable('projects_passwords');
        $this->dropColumn('users_passwords','project_id');
    }
}
