<?php

use yii\db\Migration;

class m160330_102250_renameTables extends Migration
{
    public function safeUp()
    {
        $this->renameTable('users_projects','user_project');
        $this->renameTable('users','user');
        $this->renameTable('projects','project');
        $this->renameTable('passwords','password');
        return true;
    }

    public function safeDown()
    {
        $this->renameTable('user_project','users_projects');
        $this->renameTable('user','users');
        $this->renameTable('project','projects');
        $this->renameTable('password','passwords');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
