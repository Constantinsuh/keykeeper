app.controller('LoginController', ['$scope', '$http' ,'$window','$location', function($scope, $http, $window, $location) {

  $scope.login = '';
  $scope.password = '';

  $scope.init = function() {
    if($scope.isLogged()) {
      $location.path('/keys');
    }
  };

  $scope.onLogin = function() {
    $http({
      method: 'POST',
      url: 'site/login',
      data: {
        login: $scope.login,
        password: $scope.password,
        _csrf: $('meta[name="csrf-token"]').attr("content")
      }
    }).then(function successCallback(response) {
      if(response.data.result === 'success') {
        $window.sessionStorage.access_token = response.data.access_token;
        $scope.getUserInfo();
        $location.path('/keys');
      } else {
        console.log('Wrong pw');
      }

    }, function errorCallback(response) {
      console.log(response);
    });
  };



  $scope.init();
}]);

