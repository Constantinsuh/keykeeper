app.controller('UserController', ['$scope', '$http', 'User', function($scope, $http, User) {

  $scope.isCreate = true;

  $scope.user = new User();
  $scope.usersList = [];
  $scope.selectedUser = null;

  $scope.init = function() {
    var callback = function(response) {
      angular.forEach(response.data, function(item, key) {
        $scope.usersList.push(new User(item));
      })
    };
    new User().viewAll(callback);
  };

  $scope.onRegister = function() {
    $scope.user.create(function(){});
  };

  $scope.onDeleteClick = function(userModel) {
    if(confirm('Confirm delete "'+userModel.name+'"')) {
      //var callback
      userModel.delete();
    }
  };


  $scope.init();
}]);
