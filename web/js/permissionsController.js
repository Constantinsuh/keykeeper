
app.controller('PermissionController', ['$scope', 'User', 'UserProject', 'Project', function($scope, User, UserProject, Project) {

  $scope.usersList = [];
  $scope.projectsList = [];
  $scope.selectedUser = null;

  $scope.init = function() {
    var callback = function(response) {
      angular.forEach(response.data, function(item, key) {
        $scope.usersList.push(new User(item));
      })
    };
    new User().viewAll(callback);


    var callbackProjects = function(response) {
      var newProject;
      angular.forEach(response.data, function(item) {
        newProject = new Project(item);
        newProject.userProject = new UserProject({
          project_id: newProject.id
        });
        $scope.projectsList.push(newProject);
      });
      console.log($scope.projectsList);
    };
    new Project().viewAll(callbackProjects);
  };

  $scope.onUserSelected = function() {
    angular.forEach($scope.projectsList, function(item) {
      item.userProject = new UserProject({
        user_id: $scope.userInfo.id,
        project_id: item.id
      });
    });



    var callback = function(response) {
      console.log(response);
      angular.forEach(response.data, function(userProject) {
        angular.forEach($scope.projectsList, function(project) {
          if(userProject.project_id === project.id) {
            project.userProject = new UserProject(userProject);
          }
        });
      });
    };
    new UserProject().viewAllByUser(callback, $scope.selectedUser.id);
  };

  $scope.onSavePermissionsClick = function() {
    angular.forEach($scope.projectsList, function(item) {
      item.userProject.user_id = $scope.selectedUser.id;
      item.userProject.updateIfChanged();
    });
  };




  $scope.init();
}]);