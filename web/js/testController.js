/*

 */

app.controller('TestController', ['$scope', '$http', 'Password', 'Project', function($scope, $http, Password, Project) {

  $scope.modelsList = [Password, Project];
  $scope.selectedModel = null;

  $scope.methodsList = ['update' ,'create', 'delete'];
  $scope.selectedMethod = null;
  $scope.modelAttributes = {};

  $scope.onModelSelected = function() {
    $scope.selectedModel = new $scope.selectedModel();
  };

  $scope.onSubmitModel = function() {
    var callback = function(response) {
      console.log(response);
    };
    $scope.selectedModel[$scope.selectedMethod](callback);
  }






}]);