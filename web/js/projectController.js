app.controller('ProjectController', ['$scope', '$window','$http', 'Password', 'Project', 'UserProject',
  function($scope, $window, $http, Password, Project, UserProject) {

  $scope.userKeys = [];
  $scope.userProjects = [];
  $scope.additionalProject = new Project();

  $scope.init = function() {
    $scope.getAllAvailableProjects();
  };

  //READ
  $scope.getAllAvailableProjects = function() {
    $http({
      method: 'GET',
      url: 'user-project/get-own-projects'
    }).then(function successCallback(response) {
      if(response.data) {
        angular.forEach(response.data, function(item, i) {
          var newUserProject = new UserProject(item);
          newUserProject.passwords = [];

          newUserProject.project = new Project(item.project);
          newUserProject.additionalKey = new Password();

          angular.forEach(item.passwords, function(item, i) {
            newUserProject.passwords.push(new Password(item));
          });
          $scope.userProjects.push(newUserProject);

        });
      }
    });
  };

  $scope.onToggleProjectClick = function(projectModel, event) {
  };

  $scope.onCancelEditClick = function(passwordsModel) {
    passwordsModel.setOldAttributes();
  };

  $scope.onEditKeyClick = function(passwordsModel) {
    passwordsModel._isEditable = true;
  };

  $scope.onShowPasswordClick = function(passwordModel, event) {
    passwordModel._passwordToShow = passwordModel.password;

    /* Timeout require, because we need select text only after html changes,
     * that was called by $apply();
     */
    setTimeout(function() {
      var target = event.currentTarget;
      var rng, sel;
      rng = document.createRange();
      rng.selectNode(target);
      sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(rng);
    }, 20);

  };

  //UPDATE
  $scope.onSaveKeyClick = function(passwordsModel) {
    var callback = function(response) {
      passwordsModel._isEditable = false;
    };
    passwordsModel.update(callback);
  };


  //ADD
  $scope.onAddNewPasswordClick = function(passwordsModel, project) {
    passwordsModel.project_id = project.project.id;
    var callback = function(response) {
      var newPassword = new Password(response.data);
      project.passwords.push(newPassword);
      project.additionalKey.setDefaultValues();
    };
    passwordsModel.create(callback);
  };

  //DELETE
  $scope.onKeyDeleteClick = function(passwordsModel, project, key) {
    if(confirm('Confirm delete "'+passwordsModel.description+'"')) {
      var callback = function(response) {
        project.passwords.splice(key, 1);
      };
      passwordsModel.delete(callback);
    }
  };

  /* Project */

  $scope.onProjectCancelEditClick = function(projectModel) {
    projectModel.setOldAttributes();
  };

  $scope.onProjectEditKeyClick = function(projectModel) {
    projectModel._isEditable = true;
  };

  //PROJECT DELETE
  $scope.onProjectDeleteKeyClick = function(projectModel, key) {
    if(confirm('Confirm delete "'+projectModel.name+'"')) {
      var callback = function (response) {
        $scope.userProjects.splice(key, 1);
      };
      projectModel.delete(callback);
    }
  };


  //PROJECT UPDATE
  $scope.onProjectSaveKeyClick = function(projectModel) {
    var callback = function(response) {
      projectModel._isEditable = false;
    };
    projectModel.update(callback);
  };


  //PROJECT CREATE
  $scope.onAddNewProjectClick = function(projectModel) {
    var callback = function(response) {
      var newProject = new Project(response.data);
      var projectModel = new $scope.projectModel(Object);
      projectModel.project = newProject;
      $scope.userProjects.push(projectModel);
    };
    projectModel.create(callback);
  };


  $scope.onChangePasswordFieldTypeClick = function(passwordsModel) {
    if(passwordsModel._passwordFieldType === 'password') {
      passwordsModel._passwordFieldType = 'text';
      passwordsModel._changePasswordButtonTypeText = 'Hide';
    } else if(passwordsModel._passwordFieldType === 'text') {
      passwordsModel._passwordFieldType = 'password';
      passwordsModel._changePasswordButtonTypeText = 'Show';
    }
  };


  $scope.projectModel = function(params) {
    return {
      project : new Project(params.project) || Object,
      passwords: params.passwords || []
    }
  };

  $scope.init();

}]);