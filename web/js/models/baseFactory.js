app.factory('BaseFactory',['$http', '$rootScope', function($http, $rootScope){

  function BaseFactory(data) {
    this.setDefaultValues();
    this.oldAttributes =  angular.extend({},this.defaultValues);
    if (data) {
      this.setAttributes(data);
      angular.extend(this.oldAttributes, data);
    }
    return this;
  }

  BaseFactory.prototype = {
    //model name
    modelName: '',

    //url to server controller
    modelUrl: '',

    //attributes values, from last server update
    oldAttributes: {},

    //attributes values by default
    defaultValues: {},

    //public attributes, available for mass assignment
    publicAttributes: [],


    setDefaultValues: function() {
      var that = this;
      angular.forEach(this.defaultValues, function(value, key){
        this[key] = value;
      }, this);
    },

    setOldAttributes: function() {
      angular.forEach(this.oldAttributes, function(value, key){
        this[key] = value;
      }, this);
    },

    setAttributes: function(passwordsData) {
      angular.forEach(this.publicAttributes, function(item) {
        if(passwordsData[item] != undefined) {
          this[item] = passwordsData[item];
        }
      }, this);
    },

    getAttributes: function() {
      var arr = {};
      angular.forEach(this.publicAttributes, function(item) {
        arr[item] = this[item];
      }, this);
      return arr;
    },

    viewAll: function(callback) {
      var data = {};
      data._csrf = $('meta[name="csrf-token"]').attr("content");
      $http({
        method: 'GET',
        url: this.modelUrl,
        data: data
      }).then(function successCallback(response) {
        if(callback) {
          callback(response);
        }
      });
    },

    update: function(callback) {
      var data = this.getAttributes();
      var that = this;
      data._csrf = $('meta[name="csrf-token"]').attr("content");
      $http({
        method: 'PUT',
        url: this.modelUrl + '/'+that.id,
        data: data
      }).then(function successCallback(response) {
        $rootScope.showAlertBottomMessage(that.modelName + ' updated successful', 'success');
        that.setAttributes(response.data);
        angular.extend(that.oldAttributes, response.data);
        if(callback) {
          callback(response);
        }
      })
    },

    create: function(callback) {
      var data = this.getAttributes();
      var that = this;
      data._csrf = $('meta[name="csrf-token"]').attr("content");
      $http({
        method: 'POST',
        url: this.modelUrl,
        data: data
      }).then(function successCallback(response) {
        $rootScope.showAlertBottomMessage('New '+that.modelName+' created successful', 'success');
        angular.extend(that.oldAttributes, response.data);
        if(callback) {
          callback(response);
        }
      })
    },

    delete : function(callback) {
      var data = this.getAttributes();
      var that = this;
      data._csrf = $('meta[name="csrf-token"]').attr("content");
      $http({
        method: 'DELETE',
        url: this.modelUrl + '/' + this.id
      }).then(function successCallback(response) {
        $rootScope.showAlertBottomMessage(that.modelName + ' deleted successful', 'success');
        if(callback) {
          callback(response);
        }
      })
    },

  };
  return BaseFactory;

}]);