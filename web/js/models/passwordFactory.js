app.factory('Password',[ 'BaseFactory', function(BaseFactory){

  var Password = function() {
    BaseFactory.apply(this, arguments);
  };
  Password.prototype = new BaseFactory();

  angular.extend(Password.prototype, {
    modelName: 'Password',
    modelUrl: 'password',

    defaultValues: {
      id: null,
      description: null,
      password: null,
      project_id: null,
      _passwordToShow: '*********',
      _isEditable: false,
      _passwordFieldType: 'text',
      _changePasswordButtonTypeText: 'Hide'
    },

    publicAttributes: ['id','description','password','project_id']


  });

  return Password;

}]);