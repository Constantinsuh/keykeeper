app.factory('User',['BaseFactory', function(BaseFactory) {

  var User = function() {
    BaseFactory.apply(this, arguments);
  };
  User.prototype = new BaseFactory();

  angular.extend(User.prototype, {
    modelName: 'User',
    modelUrl: 'user',

    defaultValues: {
      id: null,
      name: null,
      login: null,
      email: null,
      password: null,
      passwordConfirm: null,
      userProject: []
    },

    publicAttributes: ['id', 'name', 'login','email', 'password','passwordConfirm']

  });

  return User;
}]);