app.factory('UserProject',['$http', 'BaseFactory', function($http, BaseFactory) {

  var UserProject = function() {
    BaseFactory.apply(this,arguments);
  };

  UserProject.prototype = new BaseFactory();

  angular.extend(UserProject.prototype, {
    modelUrl: 'user-project',
    modelName: 'User Project',

    defaultValues: {
      id: null,
      user_id: null,
      project_id: null,
      can_view: 0,
      can_edit: 0
    },

    publicAttributes: ['id','user_id','project_id','can_view','can_edit'],

    //attributes, that should be checked for change in model
    attributesForChange: ['can_edit', 'can_view'],

    updateIfChanged: function() {
      if(this.isChanged()) {
        if(this.id == null) {
          this.create(function(){});
        } else {
          this.update(function(){});
        }
      }
    },

    isChanged: function() {
      var isChanged = false;
      angular.forEach(this.attributesForChange, function(item) {
        if(this.oldAttributes[item] !== this[item])  {
          isChanged = true;
        }
      }, this);
      return isChanged;
    },

    viewAllByUser: function(callback, userId) {
      $http({
        method: 'GET',
        url: this.modelUrl + '/index-by-user?userId='+userId
      }).then(function successCallback(response) {
        callback(response);
      });
    }

  });

  return UserProject;
}]);