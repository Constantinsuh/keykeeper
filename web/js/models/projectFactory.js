app.factory('Project',['$http','BaseFactory', function($http, BaseFactory){

  var Project = function() {
    BaseFactory.apply(this,arguments);
  };

  Project.prototype = new BaseFactory();

  angular.extend(Project.prototype, {
    modelName: 'Project',
    modelUrl: 'project',

    defaultValues: {
      id: null,
      name: null,
      _isEditable: false,
      _isShow: false
    },

    publicAttributes: ['id', 'name'],

  });

  return Project;
}]);