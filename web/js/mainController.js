app.controller('MainController', ['$scope', '$window','$location','$http','$rootScope', function($scope, $window, $location, $http, $rootScope) {

  $scope.alertBottomMessages = [ ];

  $rootScope.showAlertBottomMessage = function(text, type, time) {
    time = time || 3000;
    var alert = {};
    alert.text = text;
    alert.type = 'alert alert-'+type;

    $scope.alertBottomMessages.find(function(item, i) {
      if(item.text == text ) {
        $scope.alertBottomMessages.splice(i, 1);
      }
    });

    $scope.alertBottomMessages.push(alert);
    setTimeout(function() {
      $scope.alertBottomMessages.find(function(item, i) {
        if(item && item.$$hashKey == alert.$$hashKey) {
          $scope.alertBottomMessages.splice(i, 1);
          $scope.$apply();
        }
      });
    }, time);
  };

  $scope.userInfo = {
    id: null,
    name: null,
    roles: []
  };

  $scope.isAdmin = function() {
    if($scope.userInfo != null) {
      return $scope.in_array('admin', $scope.userInfo.roles);
    } else {
      return false;
    }
  };

  $scope.init = function() {
    $scope.getUserInfo();
    $scope.isAdmin();
  };

  $scope.getUserInfo = function() {
    if($scope.isLogged()) {
      $http({
        method: 'GET',
        url: 'site/get-user-info'
      }).then(function successCallback(response) {
        $scope.userInfo = response.data;
      });
    }
  };

  $scope.isLogged = function() {
    return Boolean($window.sessionStorage.access_token);
  };

  $scope.onLogout = function() {
    delete $window.sessionStorage.access_token;
    $scope.userInfo = null;
    $location.path('/login').replace();
  };

  $scope.in_array = function(value, array) {
    for(var i = 0; i < array.length; i++) {
      if(array[i] == value) return true;
    }
    return false;
  };

  $scope.init();
}]);