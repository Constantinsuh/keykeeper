var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', '$httpProvider',
  function($routeProvider, $httpProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'views/partial/LoginForm.html',
        controller: 'LoginController'
      }).
      when('/keys', {
        templateUrl: 'views/partial/keys.html',
        controller: 'ProjectController'
      }).
      when('/createUser', {
        templateUrl: 'views/partial/createUser.html',
        controller: 'UserController'
      }).
      when('/editPermissions', {
        templateUrl: 'views/partial/permissions.html',
        controller: 'PermissionController'
      }).
      when('/test', {
        templateUrl: 'views/partial/test.html',
        controller: 'TestController'
      }).
      otherwise({
        redirectTo: '/login'
      });
    $httpProvider.interceptors.push('authInterceptor');
  }]);

app.factory('authInterceptor', function ($q, $window, $location, $rootScope) {
  return {
    request: function (config) {
      if ($window.sessionStorage.access_token) {
        //HttpBearerAuth
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
      }
      return config;
    },
    responseError: function (response) {
      if (response.status === 401) {
        $location.path('/login').replace();
      }
      $rootScope.showAlertBottomMessage(response.statusText, 'danger', 5000);
      return $q.reject(response);
    }
  };
});
