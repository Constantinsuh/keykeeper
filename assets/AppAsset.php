<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'css/main.css',
    ];
    public $js = [
        'node_modules/angular/angular.js',
        'node_modules/angular-route/angular-route.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/js/tab.js',

        'app.js',
        'js/loginController.js',
        'js/mainController.js',
        'js/projectController.js',
        'js/permissionsController.js',
        'js/userController.js',
        'js/models/baseFactory.js',
        'js/models/passwordFactory.js',
        'js/models/projectFactory.js',
        'js/models/userFactory.js',
        'js/models/userProjectFactory.js',

        'js/testController.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',

    ];
}
